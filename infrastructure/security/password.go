package security

import (
	"fmt"

	"github.com/alexandrevicenzi/unchained"
)

func MakePassword(plainPassword string) (hash string, err error) {
	hash, err = unchained.MakePassword(plainPassword, unchained.GetRandomString(12), "default")
	if err != nil {
		return "", err
	}
	return hash, err
}

func CheckPassword(hashedPassword string, password string) (result bool, err error) {
	result, err = unchained.CheckPassword(password, hashedPassword)
	return result, err
}

func VerifyPassword(password string, confirmPassword string) (bool, error) {
	if password != "" && confirmPassword != "" {
		if password == confirmPassword {
			return true, nil
		}
	}
	return false, fmt.Errorf("Password mismatch")
}
