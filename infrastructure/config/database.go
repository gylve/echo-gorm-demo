package config

import (
	"fmt"

	"gitlab.com/gylve/echo-gorm-demo/domain/entity"
	"gitlab.com/gylve/echo-gorm-demo/domain/repository"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/persistence"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Repositories struct {
	User repository.UserRepository
	db   *gorm.DB
}

// constructor
func NewRepositories() (*Repositories, error) {
	db, err := gorm.Open(postgres.Open(getPostgreSQLDSN()), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return &Repositories{
		User: persistence.NewUserRepository(db),
		db:   db,
	}, nil

}

//  This function returns the Database url connection
func getPostgreSQLDSN() string {

	// Get value from environment variable. If not set default value
	user := getEnv(PostgreSQLUSER, "demo")
	pwd := getEnv(PostgreSQLPASSWORD, "demo")
	port := getEnv(PostgreSQLPORT, "5432")
	db := getEnv(PostgreSQLDB, "demo")
	host := getEnv(PostgreSQLHOST, "localhost")
	return fmt.Sprintf("user=%s password=%s host=%s dbname=%s port=%s sslmode=disable", user, pwd, host, db, port)
}

func (s *Repositories) ApplyMigrations() error {
	return s.db.AutoMigrate(&entity.User{})
}
