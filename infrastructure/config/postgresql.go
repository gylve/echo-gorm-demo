package config

import (
	"gitlab.com/gylve/echo-gorm-demo/domain/entity"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var PostgreSQLConn *gorm.DB

func NewPostgreSQLConnection() (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(getPostgreSQLDSN()), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return db, err
}

// func getPostgreSQLDSN() string {

// 	// Get value from environment variable. If not set default value
// 	user := getEnv(PostgreSQLUSER, "demo")
// 	pwd := getEnv(PostgreSQLPASSWORD, "demo")
// 	port := getEnv(PostgreSQLPORT, "5432")
// 	db := getEnv(PostgreSQLDB, "demo")
// 	host := getEnv(PostgreSQLHOST, "localhost")
// 	return fmt.Sprintf("user=%s password=%s host=%s dbname=%s port=%s sslmode=disable", user, pwd, host, db, port)
// }

func ApplyMigrations() error {
	return PostgreSQLConn.AutoMigrate(&entity.User{})
}
