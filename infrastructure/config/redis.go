package config

import redis "github.com/go-redis/redis"

var RedisConn *redis.Client

func NewRedisConnection() (*redis.Client, error) {
	connection := redis.NewClient(&redis.Options{
		Addr:     getEnv(RedisHOST, "localhost") + ":" + getEnv(RedisPORT, "6379"),
		Password: getEnv(RedisPASSWORD, ""), // no password set
		DB:       0,                         // use default DB
	})
	_, err := connection.Ping().Result()
	return connection, err
}
