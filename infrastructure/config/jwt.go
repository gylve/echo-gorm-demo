package config

import (
	"crypto/rsa"
	"io/ioutil"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/gommon/log"
)

var (
	PrivateKey *rsa.PrivateKey
	PublicKey  *rsa.PublicKey
)

func init() {
	var privateBytes, publicBytes []byte
	var err error
	privateBytes, err = ioutil.ReadFile("./infrastructure/config/certificates/private.rsa")
	if err != nil {
		log.Fatal("Private Key not Found")
	}
	publicBytes, err = ioutil.ReadFile("./infrastructure/config/certificates/public.rsa.pub")
	if err != nil {
		log.Fatal("Public Key not Found!")
	}
	PrivateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	PublicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
}

type JwtCustomClaims struct {
	UUID   string `json:uuid`
	Active bool   `json:active`
	jwt.StandardClaims
}
