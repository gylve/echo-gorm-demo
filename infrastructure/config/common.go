package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
)

const (
	// Environment Value
	GOENV = "GOENV"

	// Database Config
	PostgreSQLDB       = "PostgreSQL_DB"
	PostgreSQLHOST     = "PostgreSQL_HOST"
	PostgreSQLPORT     = "PostgreSQL_PORT"
	PostgreSQLUSER     = "PostgreSQL_USER"
	PostgreSQLPASSWORD = "PostgreSQL_PASSWORD"

	// Redis Config
	RedisHOST     = "REDIS_HOST"
	RedisPORT     = "REDIS_PORT"
	RedisPASSWORD = "REDIS_PASSWORD"
)

func LoadEnv() {

	// Local setting if environment variable is not set
	if os.Getenv(GOENV) == "" {
		os.Setenv(GOENV, "development")
	}

	err := godotenv.Load(fmt.Sprintf(".env.%s", os.Getenv(GOENV)))
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func getEnv(name, def string) string {
	env := os.Getenv(name)
	if len(env) != 0 {
		return env
	}
	return def
}
