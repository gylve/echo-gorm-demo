package auth

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/config"
)

func GenerateTokenPair(UUID string) (map[string]string, error) {

	// Set Claims
	claims := &config.JwtCustomClaims{
		UUID:   UUID,
		Active: true,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 12).Unix(),
		},
	}

	// Making JWT Token
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	t, err := token.SignedString(config.PrivateKey)
	if err != nil {
		return nil, err
	}

	refreshToken := jwt.New(jwt.SigningMethodRS256)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["Active"] = true
	rtClaims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	rt, err := refreshToken.SignedString(config.PrivateKey)
	if err != nil {
		return nil, err
	}

	return map[string]string{
		"access_token":  t,
		"refresh_token": rt,
	}, nil

}
