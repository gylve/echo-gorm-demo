package entity_test

import (
	"fmt"
	"testing"

	"gitlab.com/gylve/echo-gorm-demo/domain/entity"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/config"
)

func TestCreate(t *testing.T) {
	var err error
	config.PostgreSQLConn, err = config.NewPostgreSQLConnection()
	if err != nil {
		panic(err)
	}

	u := entity.User{
		FirstName: "John",
		LastName:  "Doe",
		Email:     "john@doe.com",
		Password:  "Demo123**",
		TimeZone:  "America/Guayaquil",
	}
	result := config.PostgreSQLConn.Create(&u)
	if result.Error != nil {
		fmt.Println(fmt.Errorf("Error al ingresar datos a la base %s", result.Error))

	}
	fmt.Println("Success")
	// assert.Equal(t, )
}
