package entity

import (
	"strings"

	"github.com/badoux/checkmail"
	uuid "github.com/google/uuid"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/security"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	UUID      uuid.UUID `json:"uuid" gorm:"type:uuid;"`
	FirstName string    `json:"first_name" gorm:"size:100;not null;"`
	LastName  string    `json:"last_name" gorm:"size:100;not null;"`
	Email     string    `json:"email" gorm:"size:100;not null;unique"`
	Password  string    `json:"password" gorm:"size:100;not null;"`
	TimeZone  string    `json:"timezone" gorm:"size:100;not null;"`
}
type Users []User

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	// Making UUID Code
	u.UUID = uuid.New()
	// Hashing the password
	hashedPassword, err := security.MakePassword(u.Password)
	if err != nil {
		return
	}
	u.Password = hashedPassword
	return nil
}

func (u *User) Validate(action string) map[string]string {
	var errorMessages = make(map[string]string)
	var err error

	switch strings.ToLower(action) {
	case "login":
		if u.Password == "" {
			errorMessages["password_required"] = "password is required"
		}
		if u.Email == "" {
			errorMessages["email_required"] = "email is required"
		}
		if u.Email != "" {
			if err = checkmail.ValidateFormat(u.Email); err != nil {
				errorMessages["invalid_email"] = "please provide a valid email"
			}
		}
	default:
		if u.FirstName == "" {
			errorMessages["firstname_required"] = "first name is required"
		}
		if u.LastName == "" {
			errorMessages["lastname_required"] = "last name is required"
		}
		if u.Password == "" {
			errorMessages["password_required"] = "password is required"
		}
		if u.Password != "" && len(u.Password) < 6 {
			errorMessages["invalid_password"] = "password should be at least 6 characters"
		}
		if u.Email == "" {
			errorMessages["email_required"] = "email is required"
		}
		if u.Email != "" {
			if err = checkmail.ValidateFormat(u.Email); err != nil {
				errorMessages["invalid_email"] = "please provide a valid email"
			}
		}
	}
	return errorMessages
}
