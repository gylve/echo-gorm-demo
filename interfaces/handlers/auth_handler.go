package handlers

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/gylve/echo-gorm-demo/application"
	"gitlab.com/gylve/echo-gorm-demo/domain/entity"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/auth"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/config"
)

type Authenticate struct {
	us application.UserAppInterface
}

type userParams struct {
	Email           string `json:"email" query:"email"`
	Password        string `json:"password" query:"password"`
	ConfirmPassword string `json:"confirm_password" query:"confirm_password"`
}

func NewAuthenticate(uApp application.UserAppInterface) *Authenticate {
	return &Authenticate{
		us: uApp,
	}
}

// Register new User [NOT IMPLEMENTED]
// func (up userParams) Register(c *echo.Context) {

// }

// Login Handler
func (au *Authenticate) Login(c echo.Context) (err error) {
	cred := new(userParams)
	if err = c.Bind(cred); err != nil {
		return
	}
	user := entity.User{
		Email:    cred.Email,
		Password: cred.Password,
	}
	//validate request:
	validateUser := user.Validate("login")
	if len(validateUser) > 0 {
		return c.JSON(http.StatusUnprocessableEntity, validateUser)
	}
	u, userErr := au.us.GetUserByEmailAndPassword(&user)
	if userErr != nil {
		return c.JSON(http.StatusInternalServerError, userErr)
	}
	tokens, err := auth.GenerateTokenPair(u.UUID.String())
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, tokens)

}

// Refresh Token Handler
func (au *Authenticate) RefreshToken(c echo.Context) error {
	type tokenRequestBody struct {
		Refresh string `json: "refresh" query: "refresh"`
	}
	tokenReq := new(tokenRequestBody)
	if err := c.Bind(tokenReq); err != nil {
		return err
	}

	token, err := jwt.Parse(tokenReq.Refresh, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected Signing Method: %v", token.Header["alg"])
		}
		return config.PublicKey, nil
	})
	if err != nil {
		return err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// Get the user record from database or
		// run through your business logic to verify if the user can
		if claims["Active"] == true {
			username := fmt.Sprintf("%s", claims["UUID"])
			newTokenPair, err := auth.GenerateTokenPair(username)
			if err != nil {
				return err
			}
			return c.JSON(http.StatusOK, newTokenPair)
		}
		return echo.ErrUnauthorized
	}

	return err
}

// Logout Handler [Not Implemented]
// func (up userParams) Logout(c *echo.Context) error {
// 	return nil
// }
