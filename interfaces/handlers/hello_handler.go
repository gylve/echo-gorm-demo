package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func HelloFromEcho(c echo.Context) error {
	return c.String(http.StatusOK, "Hello From Echo")
}

func Hello(c echo.Context) error {
	return c.JSON(http.StatusOK, "Hello From Echo")
}
