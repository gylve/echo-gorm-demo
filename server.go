//
package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/gylve/echo-gorm-demo/infrastructure/config"
	"gitlab.com/gylve/echo-gorm-demo/interfaces/handlers"
)

func init() {
	// Call envfile
	config.LoadEnv()
	var err error

	// Redis Connection
	config.RedisConn, err = config.NewRedisConnection()
	if err != nil {
		panic(err)
	}
	defer config.RedisConn.Close()
}

func main() {
	// DataBase Config

	services, err := config.NewRepositories()
	if err != nil {
		panic(err)
	}
	// users := interfaces.NewUsers(services.User)
	authenticate := handlers.NewAuthenticate(services.User)

	e := echo.New()
	// Enable Debug mode
	e.Debug = true

	// Middlewares
	e.Use(middleware.CORS())
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Pre(middleware.AddTrailingSlash())
	// Restricted group config
	r := e.Group("/api/v1")
	config := middleware.JWTConfig{
		Claims:        &config.JwtCustomClaims{},
		SigningKey:    config.PublicKey,
		SigningMethod: "RS256",
	}
	r.Use(middleware.JWTWithConfig(config))

	// Routes
	e.GET("/", handlers.HelloFromEcho) // This is a demo handler
	//authentication routes
	e.POST("/login/", authenticate.Login)
	// e.POST("/logout", authenticate.Logout)
	e.POST("/refresh/", authenticate.RefreshToken)

	// Restricted Groups
	//user routes
	// r.POST("/users", users.SaveUser)
	// r.GET("/users", users.GetUsers)
	// r.GET("/users/:user_id", users.GetUser)
	r.GET("/hello/", handlers.HelloFromEcho) // This is a demo handler

	// Start server
	e.Logger.Fatal(e.Start(":8086"))

}
