#!/bin/bash

# This file Gens the public and private keys
# for sign your tokens

cd infrastructure/config/

mkdir certificates

cd certificates/

openssl genrsa -out private.rsa 2048

sleep 6

openssl rsa -in private.rsa -pubout >public.rsa.pub
